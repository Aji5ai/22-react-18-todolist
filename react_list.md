# React - Liste de composants

Apprend à afficher une liste de composants avec React

## Ressources

- [https://gitlab.com/bastienapp/react18-todolist](https://gitlab.com/bastienapp/react18-todolist)
- [React - Rendering Lists](https://react.dev/learn/rendering-lists)

## Contexte du projet

Une collègue a démarré une application, et n'a pas eu le temps de la finir. Il s'agirait d'afficher une liste de choses à faire (*todo list*).

Elle te donne le lien de son dépôt et te demande de le finir en son absence. Afin de t'aider, elle a commenté les endroits où tu devrais intervenir.

Commence par faire un fork du dépôt suivant : [https://gitlab.com/bastienapp/react18-todolist](https://gitlab.com/bastienapp/react18-todolist).

Ensuite clone ton fork en local.

​Voici ce qui a déjà été fait par ta collègue :

- `src/App.jsx` : contient un tableau d'objet todos, qui ont chacun un identifiant, un titre, un statut "est complété". Ce tableau est envoyé à TodoList.jsx par les props.
- `src/TodoList.jsx` : contient du code pour l'affichage de la liste, mais le composant est incomplet
- `src/TodoItem.jsx` : contient le code nécessaire à l'affichage d'une tâche : son titre, et si la tâche a été réalisé ou non

Tu dois modifier le composant `TodoList` afin d'afficher la liste des tâches, contenues dans la variables todos.

Ajoute des commentaires afin d'expliquer ton code.

Aide-toi de cette ressource : [React - Rendering Lists](https://react.dev/learn/rendering-lists).

Fait bien attention qu'aucune erreur **"Warning: Each child in a list should have a unique key prop."** n'apparaisse dans le terminal de ton navigateur.

Si l'erreur apparaît, aide-toi de Google pour la résoudre !

## Modalités pédagogiques

- Un dépôt GitLab contient le code du projet
- Faire un fork du dépôt et cloner le fork en local
- Modifier le composant "TodoList" afin d'afficher la liste des tâches
- Vérifier qu'aucune erreur n'apparaît dans le terminal

## Modalités d'évaluation

- La liste s'affiche bien à partir de l'objet todos
- Des commentaires expliquent le code
- Aucune erreur n'apparaît dans le terminal

## Livrables

- Un lien vers GitLab

## Critères de performance

- Le code source est documenté
- Utiliser les normes de codage du langage
- La documentation technique de l’environnement de travail est comprise
- Utiliser un outil de gestion de versions