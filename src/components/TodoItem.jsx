import PropTypes from "prop-types";

function TodoItem(props) {
  const { title, completed } = props; /* On récupère pour chaque objet du tableau son titre et statut */
  return (
    <li>
      <span>{title}</span> ({completed ? "is done" : "to do"})
    </li>
  );
}

/* .propTypes : permet de spécifier quels types de valeurs sont attendus pour chaque prop et fournit une vérification lors de l'utilisation du composant. 
Nécessaire de l'importer en début de script */
TodoItem.propTypes = {
  title: PropTypes.string.isRequired,
  completed: PropTypes.bool.isRequired,
};

export default TodoItem;
