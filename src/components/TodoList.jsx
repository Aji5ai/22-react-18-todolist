import PropTypes from "prop-types";
import TodoItem from "./TodoItem.jsx";

function TodoList(props) {
  const {todos} = props;
  return (
    <div>
      <h1>My todo list ({todos.length} items):</h1>
      <ul> {/* todos/props c'est mon tableau d'objets todos défini dans App.jsx */}
        {todos.map((currentElement) => ( /* On boucle sur le tableau et pour chaque objet on applique le composant TodoItem qui prends en props les clé des objets */
          <TodoItem key={currentElement.id} {...currentElement} /> /* Il vaut mieux utiliser l'id que l'index qui peut changer ensuite */
        ))}
      </ul>
    </div>
  );
}

/* .propTypes : permet de spécifier quels types de valeurs sont attendus pour chaque prop et fournit une vérification lors de l'utilisation du composant. 
Nécessaire de l'importer en début de script */
TodoList.propTypes = {
  todos: PropTypes.array.isRequired,
};

export default TodoList;
